﻿import java.util.Scanner;

class Fahrkartenautomat {
	
	public static double fahrkartenbestellungErfassen(Scanner tastatur) {
		System.out.print("Preis pro Ticket (EURO): ");
		return tastatur.nextDouble();		
	}
	
	public static int anzahlFahrkarten(Scanner tastatur) {
		System.out.print("Wieviele Tickets wollen Sie kaufen: ");	
		return tastatur.nextInt();
	}
	
	public static double fahrkartenBezahlen(double zuZahlen, Scanner tastatur) {
		double ergebnis = 0.0;
		double eingeworfeneMünze;
		while (ergebnis < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - ergebnis));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			ergebnis += eingeworfeneMünze;
		}
		return ergebnis;
	}
	
	public static void fahrkartenAusgeben(int millisekunde) {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben(double eingezahltG, double zuZahlen) {
		double rückgabebetrag = eingezahltG - zuZahlen;
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				//System.out.println("2 EURO");
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				//System.out.println("1 EURO");
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
			//	System.out.println("50 CENT");
				muenzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				//System.out.println("20 CENT");
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				//System.out.println("10 CENT");
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				//System.out.println("5 CENT");
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}
	
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlen = fahrkartenbestellungErfassen(tastatur) * anzahlFahrkarten(tastatur);
		double eingezahltG = fahrkartenBezahlen(zuZahlen, tastatur);
		fahrkartenAusgeben(250);
		rueckgeldAusgeben(eingezahltG,zuZahlen);
		tastatur.close();
	}
}
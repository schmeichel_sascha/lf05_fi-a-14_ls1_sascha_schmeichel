
public class Konfiguration {
	public static void main(String[] args) {

		int muenzenCent = 1280;
		int muenzenEuro = 130;
		int euro;
		double patrone = 46.24;
		String bezeichnung = "Q2021_FAB_A";
		char sprachModul = 'd';
		boolean status;
		String typ = "Automat AVR";
		String name;
		name = typ + " " + bezeichnung;
		final byte PRUEFNR = 4;
		double maximum = 100.00;
		double prozent;
		prozent = maximum - patrone;
		int cent;
		int summe;
		summe = muenzenCent + muenzenEuro * 100;
		euro = summe / 100;
		cent = summe % 100;

		status = (euro <= 150) && (cent != 0) && (sprachModul == 'd') && (euro >= 50) && (prozent >= 50.00)
				&& (!(PRUEFNR == 5 || PRUEFNR == 6));

		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Pr�fnummer : " + PRUEFNR);
		System.out.println("F�llstand Patrone: " + prozent + " %");
		System.out.println("Summe Euro: " + euro + " Euro");
		System.out.println("Summe Rest: " + cent + " Cent");
		System.out.println("Status: " + status);
	}
}

public class Ausgabenuebung2 {

	public static void main(String[] args) {
		
		//Aufgabe 1
		String stern = "**";
		System.out.printf( "%5s\n", stern );
		System.out.printf( "%1.1s %6.1s \n", stern , stern );
		System.out.printf( "%1.1s %6.1s \n", stern , stern );
		System.out.printf( "%5s\n\n", stern );

		//Aufgabe 2
		String multipli = "1 * 2 * 3 * 4 * 5";
		String gleich = "=";
		
		System.out.printf( "%s %3s %1.0s %17s %3s\n", "0!" , gleich , multipli , gleich, "1");
		System.out.printf( "%s %3s %1.1s %17s %3s\n", "1!" , gleich , multipli , gleich, "1");
		System.out.printf( "%s %3s %1.5s %13s %3s\n", "2!" , gleich , multipli , gleich, "2");
		System.out.printf( "%s %3s %1.9s %9s %3s\n", "3!" , gleich , multipli , gleich, "6");
		System.out.printf( "%s %3s %1.13s %5s %3s\n", "4!" , gleich , multipli , gleich, "24");
		System.out.printf( "%s %3s %1s %1s %1s\n\n", "5!" , gleich , multipli , gleich, "120");
		
		//Aufgabe 3
		int[] fahrenheit = new int[5];
		fahrenheit[0] = -20;
		fahrenheit[1] = -10;
		fahrenheit[2] = 0;
		fahrenheit[3] = 20;
		fahrenheit[4] = 30;
		
		double[] celsius = new double[5];
		celsius[0] = -28.8889;
		celsius[1] = -23.3333;
		celsius[2] = -17.7778;
		celsius[3] = -6.6667;
		celsius[4] = -1.1111;
		
		System.out.printf( "%s %2s %9s\n", "Fahrenheit" , "|" , "Celsius");
		String result = new String(new char[23]).replace("\0", "-");
		System.out.printf( "%s\n", result);
		System.out.printf( "%+-1d %9s %9s\n", fahrenheit[0] , "|" , celsius[0]);
		System.out.printf( "%+-1d %9s %9s\n", fahrenheit[1] , "|" , celsius[1]);
		System.out.printf( "%+-1d %10s %9s\n", fahrenheit[2] , "|" , celsius[2]);
		System.out.printf( "%+-1d %9s %9s\n", fahrenheit[3] , "|" , celsius[3]);
		System.out.printf( "%+-1d %9s %9s\n", fahrenheit[4] , "|" , celsius[4]);
		
	}

}

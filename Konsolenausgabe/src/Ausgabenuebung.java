
public class Ausgabenuebung {

	public static void main(String[] args) {

		//Aufgabe 1
		System.out.print("Das ist ein Beispielsatz."); // Das ist ein Kommentar
		System.out.println(" Ein Beispielsatz ist das."); // Das ist ein Kommentar
		System.out.println("Das ist ein Beispielsatz."); // Das ist ein Kommentar
		System.out.println("Ein Beispielsatz ist das."); // Das ist ein Kommentar

		//Aufgabe 2
		String Stern = "*************";

		System.out.printf("%7.1s\n", Stern);
		System.out.printf("%8.3s\n", Stern);
		System.out.printf("%9.5s\n", Stern);
		System.out.printf("%10.7s\n", Stern);
		System.out.printf("%11.9s\n", Stern);
		System.out.printf("%12.11s\n", Stern);
		System.out.printf("%13s\n", Stern);
		System.out.printf("%8.3s\n", Stern);
		System.out.printf("%8.3s\n", Stern);

		//Aufgabe 3
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;

		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e);
		
		
		// Der Baum nochmal als Schleife
		int x = 6;

		for (int i = 0; i <= x; i++) {
			String result = new String(new char[x - i]).replace("\0", " ");
			String result2 = new String(new char[1 + 2 * i]).replace("\0", "*");
			System.out.println(result + result2);
		}
		
		System.out.printf("%8.3s\n", Stern);
		System.out.printf("%8.3s\n", Stern);

	}

}

import java.util.Scanner;
public class RoemischeZahlen {

	public static void main(String[] args) {
		
		Scanner eingabewert = new Scanner(System.in);
		
		String rZahl;
		int zahl = 0;
		int[] arr = new int[7];
		
		rZahl = eingabewert.nextLine();
		
		rZahl = rZahl + ' ';
		
		for (int i = 0; i < rZahl.length() - 1; i++) {
			
			if(rZahl.charAt(i) == 'M') {
				zahl += 1000;
				arr[0] += 1;
				if(arr[0] > 3) {
					zahl = -1;
					break;
				}
			} else if(rZahl.charAt(i) == 'D') {
				zahl += 500;
				arr[1] += 1;
				if(arr[1] > 1) {
					zahl = -1;
					break;
				}
			} else if(rZahl.charAt(i) == 'C') {
				if(rZahl.charAt(i + 1) == 'D') {
					zahl += 400;
					i++;
					arr[1] += 1;
					if(arr[1] > 1) {
						zahl = -1;
						break;
					} 
				} else if (rZahl.charAt(i + 1) == 'M') {
					zahl += 900;
					i++;
					if(arr[0] > 3) {
						zahl = -1;
						break;
					}
				} else {
					zahl += 100;
					arr[2] += 1;
					if(arr[2] > 3) {
						zahl = -1;
						break;
					}
				}
			} else if(rZahl.charAt(i) == 'L') {
				zahl += 50;
				arr[3] += 1;
				if(arr[3] > 1) {
					zahl = -1;
					break;
				}
			} else if(rZahl.charAt(i) == 'X') {
				if(rZahl.charAt(i + 1) == 'L') {
					zahl += 40;
					i++;
					arr[3] += 1;
					if(arr[3] > 1) {
						zahl = -1;
						break;
					}
				} else if (rZahl.charAt(i + 1) == 'C') {
					zahl += 90;
					i++;
					arr[2] += 1;
					if(arr[2] > 3) {
						zahl = -1;
						break;
						}			
					} else if ((rZahl.charAt(i + 1) == 'M') || (rZahl.charAt(i + 1) == 'D')) {
						zahl = -1;
						break;
					} else {
					zahl += 10;
					arr[4] += 1;
					if(arr[4] > 3) {
						zahl = -1;
						break;
					}
				}
				
			} else if(rZahl.charAt(i) == 'V') {
				zahl += 5;
				arr[5] += 1;
				if(arr[5] > 1) {
					zahl = -1;
					break;
				}
			} else if(rZahl.charAt(i) == 'I') {
				if(rZahl.charAt(i + 1) == 'V') {
					zahl += 4;
					i++;
					arr[4] += 1;
					if(arr[4] > 1) {
						zahl = -1;
						break;
					}
				} else if (rZahl.charAt(i + 1) == 'X') {
					zahl += 9;
					i++;
					arr[5] += 1;
					if(arr[5] > 3) {
						zahl = -1;
						break;
					}
				} else if ((rZahl.charAt(i + 1) == 'M') || (rZahl.charAt(i + 1) == 'D') || (rZahl.charAt(i + 1) == 'C') || (rZahl.charAt(i + 1) == 'L')) {
					zahl = -1;
					break;
				} else {
					zahl += 1;	
					arr[6] += 1;
					if(arr[6] > 3) {
						zahl = -1;
						break;
					}
				}
			} else {
				zahl = -1;
				break;
			}
		}
		if(zahl == -1) {
			System.out.println("Falsche Eingabe!");
		}
		else {
			System.out.println("Die Zahl lautet " + zahl);
		}

	}

}
